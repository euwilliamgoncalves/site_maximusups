const express = require("express");
const server = express();
const nodemailer = require('nodemailer');


// upload arquivo (multer e body-parser)
const bodyParser= require('body-parser');
server.use(bodyParser.urlencoded({extended: true}));

// configurar pasta pública

server.use(express.static("public"));

// habilitar req.body

server.use(express.urlencoded({extended: true}));

// utilizando template engine

const nunjucks = require("nunjucks");
nunjucks.configure("src/views", {
    express: server,
    noCache: true
})

// dados das ups

const dadosSpecs = [
    {
        idupsh1: "Nobreak Max Basic - 1000 a 1500 VA",
        idupsim: "img/maxbasic1.png",
        idupsi1: "img/maxbasic2.png",
        idupsi2: "img/maxbasic3.png",
        idupsi3: "img/maxbasic4.png",
        vnoment: "Bivolt automático: 127 ou 220 V",
        varvent: "+/- 35% em 220 V",
        varfent: "-",
        fpotent: "-",
        conexao: "Tomada padrão NBR 14136",
        tnomsai: "115 V",
        varvsai: "+/- 10% (em modo bateria)",
        varfsai: "+/- 1% (em modo bateria)",
        trnssai: "~ 4-8 milissegundos",
        fpotsai: "-",
        fcrssai: "-",
        fondsai: "-",
        conxsai: "Tomadas padrão NBR 14136",
        bateria: "2 baterias VRLA de 7 ou 9 Ah",
        indvis1: "Entrada e Saída",
        indvis2: "Nível de carga, capacidade da bateria, freq. da rede",
        indvis3: "Não",
        psobrec: "Sim + Sobretensão de bateria e curto-circuito",
        dimenso: "126mm x 146mm x 345mm",
        pesoeqp: "11,2 kg (1000 VA) / 15 kg (1500 VA)",
        nivruid: "Menor que 45db a 1 metro",
        tempope: "0 a 40°C",
        umidade: "0-90% sem condensação",
    },
    {
        idupsh1: "Nobreak Max Plus - 3 kVA",
        idupsim: "img/maxplus1.png",
        idupsi1: "img/maxplus2.png",
        idupsi2: "img/maxplus3.png",
        idupsi3: "img/maxplus4.png",
        vnoment: "220 V (opcional 115 V)",
        varvent: "100 a 300 V",
        varfent: "56 a 64 Hz para redes em 60 Hz",
        fpotent: "Maior que 0,99",
        conexao: "Tomada padrão NBR 14136",
        tnomsai: "220 V (opcional 115 V)",
        varvsai: "+/- 1%",
        varfsai: "+/- 1% em modo bateria / 56 a 64 Hz em modo rede",
        trnssai: "Zero / 4 ms para bypass, em falha",
        fpotsai: "0,9",
        fcrssai: "3:1",
        fondsai: "Senoidal pura",
        conxsai: "Bornes ou tomadas padrão NBR 14136",
        bateria: "8 baterias internas de 7 ou 9 Ah",
        indvis1: "Entrada, Saída e Bateria",
        indvis2: "Sinótico de operação, modo de operação, capacidade da bateria, nível de carga na saída",
        indvis3: "Modo de operação, falha, bateria e carga",
        psobrec: "< 110%: permanece operando / < 130%: 1 minuto até desligar / > 130%: desliga imediatamente",
        dimenso: "348mm x 192 mm x 460 mm",
        pesoeqp: "32,2 kg",
        nivruid: "Menos de 50 db a 1 metro",
        tempope: "0 a 40°C",
        umidade: "20-90% sem condensação",
    },
    {
        idupsh1: "Nobreak Max Power - 6 e 10 kVA",
        idupsim: "img/maxpower1.png",
        idupsi1: "img/maxpower2.png",
        idupsi2: "img/maxpower3.png",
        idupsi3: "img/maxpower4.png",
        vnoment: "220 V",
        varvent: "120 a 275 V",
        varfent: "56 a 64 Hz para redes em 60 Hz",
        fpotent: "Maior que 0,99",
        conexao: "Bornes para terminal olhal (2F+T ou FNT)",
        tnomsai: "220V (opcional saída dupla: 110+110V)",
        varvsai: "+/- 1%",
        varfsai: "+/- 1% em modo bateria / 56 a 64 Hz em modo rede",
        trnssai: "Zero",
        fpotsai: "0,9",
        fcrssai: "3:1",
        fondsai: "Senoidal Pura",
        conxsai: "Bornes para terminal olhal",
        bateria: "16 baterias de 7 ou 9 Ah",
        indvis1: "Entrada, Saída e Bateria",
        indvis2: "Sinótico de operação, modo de operação, capacidade da bateria, nível de carga na saída",
        indvis3: "Modo de operação, falha, bateria e carga",
        psobrec: "< 105%: permanece operando / < 125%: 1 minuto até desligar / > 125%: desliga imediatamente",
        dimenso: "500mm x 248mm x 616mm",
        pesoeqp: "62 kg (6 kVA) / 64,2 kg (10 kVA)",
        nivruid: "Menor que 60 db a 1 metro",
        tempope: "0 a 40°C",
        umidade: "20-90% sem condensação",
    },
    {
        idupsh1: "Nobreak Trif. Zeus - 20 a 40 kVA",
        idupsim: "img/zeus1.png",
        idupsi1: "img/zeus2.png",
        idupsi2: "img/zeus3.png",
        idupsi3: "img/zeus4.png",
        vnoment: "380/220 V (3F + N + T)",
        varvent: "176 a 276 V (Fase-Neutro)",
        varfent: "56 a 64 Hz para redes em 60 Hz",
        fpotent: "Maior que 0,99",
        conexao: "Bornes",
        tnomsai: "380/220 V (3F + N + T)",
        varvsai: "+/- 1%",
        varfsai: "+/- 0,1% em modo bateria / 56 a 64Hz em modo rede",
        trnssai: "Zero / Menor que 10 ms entre Inversor e ECO Mode",
        fpotsai: "0,8",
        fcrssai: "3:1",
        fondsai: "Senoidal Pura",
        conxsai: "Bornes",
        bateria: "Sob consulta e conforme projeto",
        indvis1: "Entrada, Saída e Bateria",
        indvis2: "Sinótico de operação, modo de operação, capacidade da bateria, nível de carga na saída",
        indvis3: "Modo de operação, falha, bateria e carga",
        psobrec: "< 110%: 10 minutos até desligar / < 130%: 1 minuto até desligar / > 130%: desliga em 1 segundo",
        dimenso: "Sob consulta e conforme projeto",
        pesoeqp: "Sob consulta e conforme projeto",
        nivruid: "Menor que 65 dB a 1 metro",
        tempope: "0 a 40°C",
        umidade: "0-95% sem condensação",
    },
];

// configurando caminhos da minha aplicação

// página inicial
// req: requisição
// res: resposta

server.get("/", (req, res) => {
    return res.render("index.html");
});

server.get("/sobre", (req, res) => {
    return res.render("sobre.html");
});

server.get("/contato", (req, res) => {
    return res.render("contato.html");
});

server.get("/servicos", (req, res) => {
    return res.render("servicos.html");
});

server.get("/solucoes", (req, res) => {

    let upsData = dadosSpecs[0];

    return res.render("solucoes.html", {upsData});
});

server.get("/upsbasic", (req, res) => {

    let upsData = dadosSpecs[0];

    return res.render("solucoes.html", {upsData});
});

server.get("/upsplus", (req, res) => {

    let upsData = dadosSpecs[1];

    return res.render("solucoes.html", {upsData});
});

server.get("/upspower", (req, res) => {

    let upsData = dadosSpecs[2];

    return res.render("solucoes.html", {upsData});
});

server.get("/upszeus", (req, res) => {

    let upsData = dadosSpecs[3];

    return res.render("solucoes.html", {upsData});
});

server.post("/enviar", (req, res) => {

    // req.body - corpo do formulário

    const values = [
        req.body.name,  
        req.body.phone, 
        req.body.email, 
        req.body.state, 
        req.body.city,
        req.body.msg];
    
        async function main() {

            // create reusable transporter object using the default SMTP transport
            let transporter = nodemailer.createTransport({
            service: 'gmail',
            port: 587,
            auth: {
                user: "sitemaximusups@gmail.com", // generated ethereal user
                pass: "SenhaMax20", // generated ethereal password
            },
            });
        
            // send mail with defined transport object
            let info = await transporter.sendMail({
                from: values[2], // sender address
                to: 'maximus@maximusups.com.br', // list of receivers
                subject: 'Mensagem Enviada no site ✔', // Subject line
                text: `Nome: ${values[0]}
                Telefone: ${values[1]}
                E-mail: ${values[2]}
                Estado: ${values[3]}
                Cidade: ${values[4]}
                Mensagem: ${values[5]}`, // plain text body
            });
        
            console.log("Message sent: %s", info.messageId);
            return res.render("enviado.html");
        }
        main().catch(console.error);
        
});

// ligando o servidor

server.listen(3000);