const buttonSearch = document.querySelector(".content header .botao")

const modal = document.querySelector("#modal")
const close = document.querySelector("#modal .headermodal a")

buttonSearch.addEventListener("click", () => {
    modal.classList.remove("hide")
})

close.addEventListener("click", () => {
    modal.classList.add("hide")
})